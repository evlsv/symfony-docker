# Symfony docker

My humble supplements.

## Requirements
- [make](https://www.gnu.org/software/make/)

## Check it out
Available make commands
```bash
make
```

### Docker compose
Run containers
```bash
make up
```

Stop containers
```bash
make stop
```

### Symfony
Run Symfony console inside container
```bash
make console
```

Clear Symfony cache
```bash
make console args="cache:clear" 
```

Console make controller (yes, three \\\)
```bash
make console args="make:controller Api\\\MainController" 
```

### Composer
Composer install
```bash
make composer.install
```

Define depends on the bundle symfony/mercure-bundle
```bash
make composer args="require symfony/mercure-bundle"
```

Composer update
```bash
make composer.update
```

Find more
```bash
make help
```

## Sources
- [symfony.com/doc/current/setup/docker.html](https://symfony.com/doc/current/setup/docker.html)
- [github.com/dunglas/symfony-docker](https://github.com/dunglas/symfony-docker)
- [Makefile readme (ru)](http://linux.yaroslavl.ru/docs/prog/gnu_make_3-79_russian_manual.html)
- [Makefile readme (en)](https://www.gnu.org/software/make/manual/html_node/index.html#SEC_Contents)
