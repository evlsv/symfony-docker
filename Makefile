#!/usr/bin/make

# Makefile readme (ru): http://linux.yaroslavl.ru/docs/prog/gnu_make_3-79_russian_manual.html
# Makefile readme (en): https://www.gnu.org/software/make/manual/html_node/index.html#SEC_Contents

CURRENT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
CURRENT_TIME = $(shell date +"%F_%k-%M-%S")

include $(CURRENT_DIR)/.env

SHELL = /bin/sh
ENV_FILE = $(CURRENT_DIR)/.env
WORK_DIR = $(CURRENT_DIR)/
WWW_USER = www-data
DOCKER_COMPOSE_FILE = $(CURRENT_DIR)/docker-compose.yml
DEV = --no-dev

ifeq (dev,$(APP_ENV))
	DOCKER_COMPOSE_FILE = $(CURRENT_DIR)/docker-compose.yml
	DEV =
endif

.PHONY:
.DEFAULT_GOAL: help

# This will output the help for each task. thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## Show this help
	@printf "\033[33m%s:\033[0m\n" 'Available commands'
	@awk 'BEGIN {FS = ":.*?## "} /^[.a-zA-Z0-9_-]+:.*?## / {printf "  \033[32m%-18s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

### Docker, Docker compose
up: ## docker-compose up
	docker-compose --file $(DOCKER_COMPOSE_FILE) --env-file $(ENV_FILE) up

up.build: ## docker-compose up --build
	docker-compose --file $(DOCKER_COMPOSE_FILE) --env-file $(ENV_FILE) up --build

stop: ## docker-compose stop
	docker-compose --file $(DOCKER_COMPOSE_FILE) --env-file $(ENV_FILE) stop

down.remove-orphans: ## docker-compose down --remove-orphans
	docker-compose --file $(DOCKER_COMPOSE_FILE) --env-file $(ENV_FILE) down --remove-orphans

### Symfony commands
about: ## Symfony about
	docker-compose --file $(DOCKER_COMPOSE_FILE) --env-file $(ENV_FILE) run --rm php php bin/console about

console: ## Symfony console
	docker-compose --file $(DOCKER_COMPOSE_FILE) --env-file $(ENV_FILE) run --rm php php bin/console $(args)

### Composer
composer: ## Composer with args. Example: make composer args="require --dev phpunit/php-code-coverage"
	docker-compose --file $(DOCKER_COMPOSE_FILE) --env-file $(ENV_FILE) run --rm composer composer $(args)

composer.recipes: ## Composer recipes. Example: make composer.recipes args="symfony/console"
	docker-compose --file $(DOCKER_COMPOSE_FILE) --env-file $(ENV_FILE) run --rm composer composer recipes $(args)

composer.install: ## Composer install
	docker-compose --file $(DOCKER_COMPOSE_FILE) --env-file $(ENV_FILE) run --rm composer composer install $(DEV) --optimize-autoloader --classmap-authoritative --apcu-autoloader

composer.update: ## Composer update
	docker-compose --file $(DOCKER_COMPOSE_FILE) --env-file $(ENV_FILE) run --rm composer composer update $(DEV) --optimize-autoloader --classmap-authoritative --apcu-autoloader

composer.dumpautoload: ## Composer dump-autoload
	docker-compose --file $(DOCKER_COMPOSE_FILE) --env-file $(ENV_FILE) run --rm composer composer dump-autoload --optimize --classmap-authoritative --apcu

composer.clearcache: ## Composer dump-autoload
	docker-compose --file $(DOCKER_COMPOSE_FILE) --env-file $(ENV_FILE) run --rm composer composer clearcache

composer.show: ## Composer show
	docker-compose --file $(DOCKER_COMPOSE_FILE) --env-file $(ENV_FILE) run --rm composer composer show

composer.diagnose: ## Composer diagnose
	docker-compose --file $(DOCKER_COMPOSE_FILE) --env-file $(ENV_FILE) run --rm composer composer diagnose

composer.validate: ## Composer validate
	docker-compose --file $(DOCKER_COMPOSE_FILE) --env-file $(ENV_FILE) run --rm composer composer validate
